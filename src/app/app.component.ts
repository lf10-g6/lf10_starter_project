import {Component} from '@angular/core';
import {Employee} from "./model/Employee";
import {EmployeeDataService} from "./service/employee-data.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {TokenService} from "./service/token.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: String = 'employeeFrontendStarter';
  IsloggedIn: String = ''
  BearerToken: String = ''
  constructor(private service: TokenService) {
  }

  hasValidToken(): boolean {
     this.IsloggedIn = sessionStorage.getItem('IsLoggedIn');
    this.BearerToken = sessionStorage.getItem('BearerToken');
    if(this.IsloggedIn == 'Y' && this.BearerToken != '' && this.BearerToken != null)
    {  return true ;}
    { return false ;}
  }

}
