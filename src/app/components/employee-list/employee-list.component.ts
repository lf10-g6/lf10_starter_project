import { Component, OnInit } from '@angular/core';
import {map, Observable, of} from "rxjs";
import {Employee} from "../../model/Employee";
import {EmployeeDataService} from "../../service/employee-data.service";

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: Observable<Employee[]>
  searchedEmployee: Observable<Employee>

  constructor(private service: EmployeeDataService) {
    this.employees = this.service.getEmployees();
  }

  editEmployee: Employee = new Employee();
  addNewRow: boolean = false;
  searchMode: boolean = false;

  ngOnInit(): void {
  }

  toEdit(employee: Employee) {
    return this.editEmployee === employee;
  }

  edit(employee: Employee): void {
    this.editEmployee = employee;
  }

  cancelEdit(): void {
    this.editEmployee = new Employee();
    window.location.reload();
  }

  cancelAdd(): void {
    this.addNewRow = false
    document.getElementById("addEmployeeButton").removeAttribute("disabled");
  }

  saveEdit(employee: Employee): void {
    this.trimEmployeeValues(employee);
    if(this.isValidEmployee(employee)) {
      this.service.updateEmployee(employee);
      this.employees = this.service.getEmployees();
      this.cancelEdit();
    } else {
      alert("Eingabe unvollständig!");
    }
  }

  toAddEmptyRow(): void {
    this.addNewRow = true;
  }

  addRow(): boolean {
    if(this.addNewRow) {
      document.getElementById("addEmployeeButton").setAttribute("disabled", "disabled")
    }

    return this.addNewRow;
  }

  addEmployee(): void {
    let vorname: string = (<HTMLInputElement>document.getElementById("inputVorname")).value;
    let nachname: string = (<HTMLInputElement>document.getElementById("inputNachname")).value;
    let stadt: string = (<HTMLInputElement>document.getElementById("inputStadt")).value;
    let plz: string = (<HTMLInputElement>document.getElementById("inputPLZ")).value;
    let straße: string = (<HTMLInputElement>document.getElementById("inputStraße")).value;
    let telefon: string = (<HTMLInputElement>document.getElementById("inputTelefon")).value;

    // id muss vom Dataservice generiert werden!
    let employee : Employee = new Employee(0, nachname, vorname, straße, plz, stadt, telefon);
    this.trimEmployeeValues(employee);

    if(this.isValidEmployee(employee)) {
      this.service.addEmployee(employee)
      window.location.reload();
    } else {
      alert("Eingabe unvollständig!");
    }
  }

  trimEmployeeValues(employee: Employee): void {
    employee.firstName = employee.firstName.trim();
    employee.lastName = employee.lastName.trim();
    employee.city = employee.city.trim();
    employee.postcode = employee.postcode.trim();
    employee.street = employee.street.trim();
    employee.phone = employee.phone.trim();
  }

  isValidEmployee(employee: Employee): boolean {
    if(employee.firstName === "" ||
      employee.lastName === "" ||
      employee.city === "" ||
      employee.postcode === "" ||
      employee.postcode.length != 5 ||
      employee.street === "" ||
      employee.phone === "") {

      return false
    }

      return true
  }

  inSearch(): boolean {
    return this.searchMode;
  }

  searchEmployee(): void {
    let idString: string = (<HTMLInputElement>document.getElementById("inputSearchId")).value;

    if(idString === null || idString === undefined || idString.trim() == "") {
      this.searchMode = false;
    } else {
      let id: number = Number(idString)

      if(Number.isNaN(id)) {
        alert("Die Id muss eine Zahl sein")
      } else {
        this.searchedEmployee = this.service.getEmployee(id);

        this.searchedEmployee.subscribe({
          next: elem => this.searchMode = true,
          error: err => {
            alert("Mitarbeiter mit ID " + id + " existiert nicht!");
            this.searchMode = false;
          }
        });
      }
    }
  }

  delete(employee: Employee): void {
    this.service.deleteEmployee(employee);
    window.location.reload();
  }
}
