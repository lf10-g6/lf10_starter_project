import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import {TokenService} from "../../service/token.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  get ErrorMessage(): string {
    return this._ErrorMessage;
  }

  set ErrorMessage(value: string) {
    this._ErrorMessage = value;
  }

  get ErrorOnLogin(): boolean {
    return this._ErrorOnLogin;
  }

  set ErrorOnLogin(value: boolean) {
    this._ErrorOnLogin = value;
  }
  private _ErrorOnLogin :boolean =  false;
  private _ErrorMessage = '';


  constructor(

    private token: TokenService

   ) { sessionStorage.setItem('IsLoggedIn', 'N');}


  ngOnInit(): void {
  }

  public login() {
    let username: string = (document.getElementById('username') as HTMLInputElement).value;
    let password: string = (document.getElementById('password') as HTMLInputElement).value;
    this.token
      .Login(username,password).subscribe({
      next: BearerToken => {
        if (this.token.IsLoggedIn) {
          sessionStorage.setItem('IsLoggedIn', 'Y');
          sessionStorage.setItem('BearerToken', BearerToken.access_token);
        }},
      error: err => {this.ErrorOnLogin = true;this.ErrorMessage = 'Keine Übereinstimmung mit der Kombination aus Username & Passwort, bitte überprüfen Sie Ihre Angaben.';

      }
    });

  }}
