import {Injectable} from "@angular/core";
import {Employee} from "../model/Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {TokenService} from "./token.service";

@Injectable({
  providedIn: 'root'
})
export class EmployeeDataService {

  private bearer: string;

  constructor(private http: HttpClient, private service: TokenService) {
    this.bearer = service.getToken();
  }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).pipe(
      map(results => results.sort((a, b) => a.id - b.id))
    );
  }

  getEmployee(id: number): Observable<Employee> {
    return this.http.get<Employee>('/backend/' + id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    });
  }

  addEmployee(employee: Employee): string {
    var s: string = '';
    delete employee['id'];
    this.http.post<Employee>('/backend', employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).subscribe({
      next: () => {},
      error: (err) => s = err
    });
    return s;
  }

  updateEmployee(employee: Employee): string {
    var s: string = '';
    let id = employee.id;
    delete employee['id'];
    this.http.put<Employee>('/backend/' + id, employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).subscribe({
      next: () => {},
      error: (err) => s = err
    });
    return s;
  }

  deleteEmployee(employee: Employee): string {
    var s: string = '';
    let id = employee.id;
    delete employee['id'];
    this.http.delete<Employee>('/backend/' + id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).subscribe({
      next: () => {},
      error: (err) => s = err
    });
    return s;
  }
}
