import { Injectable } from '@angular/core';
import { catchError, Observable, tap, throwError } from "rxjs";
import { HttpErrorResponse } from "@angular/common/http";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BearerToken} from "../model/BearerToken";


@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private URL: string  = 'http://authproxy.szut.dev'
  private bearerToken: string
  private HTTPError(err: HttpErrorResponse): Observable<never> {
    return throwError(() => err);
  }
  private Http: HttpClient;
  private _IsLoggedIn: boolean = false;

  constructor(private http: HttpClient,) {
    this.Http = http
  }

  set IsLoggedIn(value: boolean) {
    this._IsLoggedIn = value;
  }

  get IsLoggedIn(): boolean {
    return this._IsLoggedIn;
  }

  setToken(Bearertoken: BearerToken): void {
    this.bearerToken = Bearertoken.access_token;

  }
  /* Login */
  Login(username: string, password: string): Observable<BearerToken> {
    let requestBody = `grant_type=password&client_id=employee-management-service&username=${username}&password=${password}`;
    return this.http.post<BearerToken>(this.URL, requestBody,
      {headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')}).pipe(tap(AuthBearerToken=>{AuthBearerToken.access_token !== '' ? this._IsLoggedIn = true : this._IsLoggedIn = false ;this.setToken(AuthBearerToken);}),catchError(this.HTTPError));}

  getToken(): string {
   if (sessionStorage.getItem('BearerToken') != null || sessionStorage.getItem('BearerToken') != '')
   {return sessionStorage.getItem('BearerToken')}
    {return ''}
  }

}
